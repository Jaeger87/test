package com.diokane.appbeamprova;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class NotificaActivity extends AppCompatActivity implements OnMapReadyCallback {


    private String type;
    private ImageView image;
    private TextView text;

    private CustomMapView mMapView;
    private GoogleMap mGoogleMap;
    private LatLng currentPosition;



    private String member;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifica);


        Intent notificationIntent = getIntent();

        type = notificationIntent.getStringExtra("type");
        member = notificationIntent.getStringExtra("member");

        image = (ImageView) findViewById(R.id.notifica_image);

        if(type.equals("caduta"))
        {
            setupCaduta(savedInstanceState);
        }
        else if(type.equals("batteria"))
        {
            setupBatteria(savedInstanceState);
        }

        else if(type.equals("silenzioso"))
        {
            setupSilenzioso(savedInstanceState);
        }


    }

    private void setupCaduta(Bundle savedInstanceState)
    {
        image.setImageResource(R.drawable.warning);

        Intent notificationIntent = getIntent();

        currentPosition = new LatLng(0 ,0);

        mMapView = findViewById(R.id.mapView_AH);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMapView.getMapAsync(this);

        mMapView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setIndoorLevelPickerEnabled(false);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);//No zoom
        mGoogleMap.setIndoorEnabled(false);
        mGoogleMap.setTrafficEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);


        //map.setOnMapClickListener(latLng -> enableFindMode(!isFindModeOn));


        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        AppConstants.REQUEST_CODE_PERMISSION_LOCATION);
            }

        } else {
            mGoogleMap.setMyLocationEnabled(true);
        }

        setMapMarker();


    }


    public void setMapMarker()
    {
        mGoogleMap.clear();

        moveToPosition(currentPosition, true);
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.pin);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, 50, 50, false);
        //In queste righe viene creato il marker da mettere sulla mappa

        mGoogleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                .anchor(0.5f, 0.5f)
                .position(currentPosition));

    }

    private void moveToPosition(LatLng position, boolean animation) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(position)
                .zoom(15).build();
        if (animation) {
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }


    private void setupBatteria(Bundle savedInstanceState)
    {
        image.setImageResource(R.drawable.low_battery);

        text = findViewById(R.id.notifica_text);

        text.setVisibility(View.VISIBLE);

        Intent notificationIntent = getIntent();
        findViewById(R.id.cardSXBottom).setVisibility(View.INVISIBLE);
        findViewById(R.id.cardDXBottom).setVisibility(View.INVISIBLE);

        text.setText("il watch di " + member + " è quasi scarico, provvedi a ricaricarlo ll prima possibile per rimanere in contatto con lui.");
    }

    private void setupSilenzioso(Bundle savedInstanceState)
    {
        image.setImageResource(R.drawable.speaker);

        text = findViewById(R.id.notifica_text);

        findViewById(R.id.cardSXBottom).setVisibility(View.INVISIBLE);
        findViewById(R.id.cardDXBottom).setVisibility(View.INVISIBLE);


        text.setVisibility(View.VISIBLE);

        Intent notificationIntent = getIntent();

        text.setText(member + " ha inserito la modalità silenziosa sul suo watch.\nSe vuoi disattivarlo puoi andare nel Widget Volumi.");
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if(mMapView != null)
            mMapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mMapView != null)
            mMapView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mMapView != null)
            mMapView.onResume();
    }

}
