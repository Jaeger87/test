package com.diokane.appbeamprova;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void caduta(View view) {

        Intent notification = new Intent(MainActivity.this, NotificaActivity.class);

        notification.putExtra("type", "caduta");
        notification.putExtra("member", "Marco");
        notification.putExtra("lat", 150.20);
        notification.putExtra("lon", 18.45);

        startActivity(notification);

    }

    public void silenzioso(View view) {


        Intent notification = new Intent(MainActivity.this, NotificaActivity.class);

        notification.putExtra("type", "silenzioso");
        notification.putExtra("member", "Marco");

        startActivity(notification);
    }

    public void Batteria(View view) {


        Intent notification = new Intent(MainActivity.this, NotificaActivity.class);

        notification.putExtra("type", "batteria");
        notification.putExtra("member", "Marco");

        startActivity(notification);
    }

}
